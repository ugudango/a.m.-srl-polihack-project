const express = require("express");
const jwt = require("jsonwebtoken");
const mongo = require("mongodb");
const mongoose = require("mongoose");
const path = require("path");
const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
const session = require("express-session");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const expressValidator = require("express-validator");

mongoose.connect("mongodb://localhost/polihack");
const db = mongoose.connection;

//Initialize routes
const routes = require("./routes/index");
const dashboard = require("./routes/dashboard");
const workspace = require("./routes/workspace");
const user = require("./routes/user");

//Initialize express
const app = express();

//Initialize Handlebars
app.set("views", path.join(__dirname, "views"));
app.engine(
  "handlebars",
  exphbs({
    defaultLayout: "main-layout"
  })
);
app.set("view engine", "handlebars");

//Initialize body-parser middleware
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

//Set static folder
app.use(express.static(path.join(__dirname, "public")));

//Express session
app.use(
  session({
    secret: "Polihack2018",
    saveUninitialized: false,
    resave: true
  })
);

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

app.use(
  expressValidator({
    errorFormatter: function(param, msg, value) {
      var namespace = param.split("."),
        root = namespace.shift(),
        formParam = root;

      while (namespace.length) {
        formParam += "[" + namespace.shift() + "]";
      }
      return {
        param: formParam,
        msg: msg,
        value: value
      };
    }
  })
);

//Linking routes
app.use("/", routes);
app.use("/dashboard", dashboard);
app.use("/workspace", workspace);
app.use("/user", user);

//Setting up port
app.set("port", process.env.PORT || 3000);

app.listen(app.get("port"), () => {
  console.log("Server started on " + app.get("port"));
});
