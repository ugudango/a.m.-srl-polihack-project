var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");

// User Schema
var UserSchema = mongoose.Schema(
  {
    username: {
      type: String,
      index: true
    },
    password: {
      type: String
    },
    workspaceIds: [String]
  },
  { collection: "users" }
);

var User = (module.exports = mongoose.model("User", UserSchema));

module.exports.createUser = function(newUser, callback) {
  if (
    newUser.username === undefined ||
    newUser.username == "" ||
    (newUser.password === undefined || newUser.password == "")
  )
    return callback({ message: "Complete all the fields!" });
  else
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(newUser.password, salt, function(err, hash) {
        newUser.password = hash;
        newUser.save(callback);
      });
    });
};

module.exports.getUserByUsername = function(username, callback) {
  var query = { username: username };
  User.findOne(query, callback);
};

module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
};

comparePassword = function(candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    if (err) throw err;
    callback(null, isMatch);
  });
};

module.exports.authenticateUser = function(candidateUser, callback) {
  module.exports.getUserByUsername(
    candidateUser.username,
    (err, verifiedUser) => {
      if (err) throw err;
      else
        comparePassword(
          candidateUser.password,
          verifiedUser.password,
          (err, isMatch) => {
            if (err) throw err;
            if (isMatch)
              return callback(
                null,
                verifiedUser,
                { message: "Successfully logged in!" },
                verifiedUser._id
              );
            else {
              console.log(verifiedUser);
              return callback(null, false, { message: "Wrong password" });
            }
          }
        );
    }
  );
};

module.exports.checkAuthenticated = function(req, res, next) {
  if (req.session && req.session.userId) {
    return next();
  } else {
    var err = new Error("You must be logged in to view this page.");
    err.status = 401;
    return res.redirect("/user/login");
  }
};

module.exports.schema = UserSchema;
