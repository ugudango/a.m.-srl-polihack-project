const mongoose = require("mongoose");
const Issue = require("../models/issue");
const User = require("../models/user");
const uuidv4 = require("uuid/v4");

//Workspace Schema
var WorkspaceSchema = mongoose.Schema(
  {
    name: {
      type: String
    },
    description: {
      type: String
    },
    userIds: [String],
    issues: [Issue.schema],
    UUID: {
      type: String
    },
    accessUUID: {
      type: String
    }
  },
  { collection: "workspaces" }
);

var Workspace = (module.exports = mongoose.model("Workspace", WorkspaceSchema));

module.exports.createWorkspace = function(newWorkspace, callback) {
  if (
    newWorkspace.name === undefined ||
    newWorkspace.name == "" ||
    newWorkspace.description === undefined ||
    newWorkspace.description == ""
  )
    return callback({ message: "Complete all the fields!" });
  else {
    newWorkspace.UUID = uuidv4();
    newWorkspace.accessUUID = uuidv4();
    newWorkspace.save(callback);
  }
};

module.exports.findByUUID = function(UUID, callback) {
  let query = { UUID };
  Workspace.findOne(query, callback);
};

module.exports.findByAccessUUID = function(UUID, callback) {
  let query = { accessUUID: UUID };
  Workspace.findOne(query, callback);
};

module.exports.generateNewUUID = function(workspaceId, callback) {
  Workspace.findById(workspaceId, function(err, workspace) {
    if (err) throw err;
    workspace.UUID = uuidv4;
    workspace.save(function(err, updatedWorkspace) {
      if (err) throw err;
      return callback(null, { message: "New UUID generated for workspace!" });
    });
  });
};

module.exports.createIssue = function(workspaceId, issueData, callback) {
  Workspace.findById(workspaceId, function(err, workspace) {
    if (err) throw err;
    let createdIssue = new Issue({
      name: issueData.name,
      description: issueData.description,
      estimate: issueData.estimate,
      asignee: issueData.asignee,
      completed: false,
      time_spent: 0,
      accessUUID: uuidv4()
    });
    workspace.issues.push(createdIssue);
    workspace.save(function(err, updatedWorkspace) {
      if (err) throw err;
      return callback(null, { message: `Issue created successfully!` });
    });
  });
};

/* module.exports.findIssueByAccessUUID = function(UUID, callback) {
	let query = { accessUUID: UUID };
	Issue.findOne(query, callback);
  }; */

module.exports.translateWorkspaceIdToName = function (workspaceId, callback) {
  Workspace.findById(workspaceId, function(err, workspace) {
    if(err) throw err;
    return callback(null, workspace)
  })
}

module.exports.completeIssue = function(
  workspaceId,
  issueUUID,
  timeSpent,
  callback
) {
  Workspace.findById(workspaceId, function(err, workspace) {
    if (err) throw err;

    for (var i = 0; i < workspace.issues.length; i++) {
      if (workspace.issues[i].accessUUID == issueUUID) {
        workspace.issues[i].completed = true;
        workspace.issues[i].time_spent = timeSpent;
        workspace.save(function(err, updatedWorkspace) {
          if (err) throw err;
          return callback(null, { message: `Issue completed successfully!` });
        });
      }
    }
  });
};
 
module.exports.joinWorkspace = function(userId, workspaceId, callback) {
  Workspace.findById(workspaceId, function(err, workspace) {
    if (err) throw err;
    workspace.userIds.push(userId);
    workspace.save(function(err, updatedWorkspace) {
      if (err) throw err;
      //res.send(updatedWorkspace);
      User.findById(userId, function(err, user) {
        if (err) throw err;
        user.workspaceIds.push(workspaceId);
        user.save(function(err, updatedUser) {
          if (err) throw err;
          //res.send(updatedUser);
          return callback(null, {
            message: `Workspace ${workspaceId} joined successfully by user ${userId}`
          });
        });
      });
    });
  });
};
