const mongoose = require("mongoose");

//Issue Schema
var IssueSchema = mongoose.Schema({
  name: {
    type: String
  },
  description: {
    type: String
  },
  estimate: {
    type: Number
  },
  asignee: {
    type: String
  },
  completed: {
      type: Boolean
  },
  time_spent: {
      type: Number
  },
  accessUUID: {
      type: String
  }
});

var Issue = (module.exports = mongoose.model("Issue", IssueSchema));
module.exports.schema = IssueSchema;