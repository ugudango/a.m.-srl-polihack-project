const express = require("express");
const router = express.Router();
const path = require("path");
const Workspace = require("../models/workspace");
const Issue = require("../models/issue");
const handlebars = require("express-handlebars");
const User = require("../models/user");

//To do: Add middleware function for checking authentication
router.post("/create", User.checkAuthenticated, (req, res) => {
  var candidateWorkspace = {
    name: req.body.name,
    description: req.body.description
  };

  let newWorkspace = new Workspace({
    name: candidateWorkspace.name,
    description: candidateWorkspace.description
  });

  Workspace.createWorkspace(newWorkspace, (err, createdWorkspace) => {
    if (err) throw err;
    console.log(createdWorkspace);
    Workspace.joinWorkspace(
      req.session.userId,
      createdWorkspace._id,
      (err, message) => {
        if (err) throw err;
        res.redirect("/dashboard/workspaces");
      }
    );
  });
});

//To do: Add middleware function for checking authentication
router.get("/join/:uuid", User.checkAuthenticated, (req, res) => {
  /*     Expected request body candidateUser:
             { ucandidateUser: {
                 _id: ..., 
                 etc...
                } 
             } */

  Workspace.findByUUID(req.params.uuid, (err, workspace) => {
    if (err) throw err;
    else
      Workspace.joinWorkspace(
        req.session.userId,
        workspace._id,
        (err, message) => {
          if (err) throw err;
          else res.redirect("/dashboard/workspaces");
        }
      );
  });
});

//To do: Add middleware function to check whether the user has access to the workspace
router.get("/:uuid", User.checkAuthenticated, (req, res) => {
  Workspace.findByAccessUUID(req.params.uuid, (err, workspace) => {
    res.render("workspace", { workspace });
  });
});

router.get("/get-workspace-data/:uuid", User.checkAuthenticated, (req, res) => {
  Workspace.findByAccessUUID(req.params.uuid, (err, workspace) => {
    res.send({ workspace });
  });
});

router.post("/get-user-by-id", (req, res) => {
  User.getUserById(req.body.userId, (err, user) => {
    res.send({ user });
  });
});

router.post("/:parentWorkspace/create-issue", User.checkAuthenticated, (req, res) => {
  //Post structure candidateIssue, parentWorkspace

  var candidateIssue = {
    name: req.body.name,
    description: req.body.description,
    estimate: req.body.estimate,
    asignee: req.body.asignee
  };

  let newIssue = new Issue({
    name: candidateIssue.name,
    description: candidateIssue.description,
    estimate: candidateIssue.estimate,
    asignee: candidateIssue.asignee
  });

  Workspace.createIssue(req.params.parentWorkspace, newIssue, err => {
    if (err) throw err;
    Workspace.findById(req.params.parentWorkspace, (err, workspace) => {
      return res.redirect("/workspace/" + workspace.accessUUID);
    })
  });
});

router.post("/complete-issue/:uuid", User.checkAuthenticated, (req, res) => {
  //Post structure parentWorkspace, candidateIssue{time_spent:...}

  Workspace.completeIssue(
    req.body.parentWorkspace,
    req.params.uuid,
    req.body.time_spent,
    (err, message) => {
      if (err) throw err;
      return res.status(200).send(message);
    }
  );
});

router.get("/translate/:id", (req, res) => {
  Workspace.translateWorkspaceIdToName(req.params.id, function(err, workspace) {
    res.status(200).send(workspace);
  });
});
module.exports = router;
