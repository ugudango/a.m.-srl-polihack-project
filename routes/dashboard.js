const express = require("express");
const router = express.Router();
const path = require("path");
const User = require("../models/user");
const Workspace = require("../models/workspace");

router.get("/get-user-info", User.checkAuthenticated, (req, res) => {
  User.getUserById(req.session.userId, (err, user) => {
    res.send({ user });
  });
});

/* router.get("/:username", (req, res) => {
  User.getUserByUsername(req.params.username, (err, user) => {
    res.render("dashboard", { user });
  });
});
 */
router.get("/", User.checkAuthenticated, (req, res) => {
  User.getUserById(req.session.userId, (err, user) => {
    if (err) throw err;
    console.log(user);
    res.render("dashboard", { user });
  });
});

router.get("/workspaces", User.checkAuthenticated, (req,res) => {
  User.getUserById(req.session.userId, (err, user) => {
    if (err) throw err;
    console.log(user);
    res.render("workspaces", { user });
  });
});

router.get("/user-profile", User.checkAuthenticated, (req,res) => {
  User.getUserById(req.session.userId, (err, user) => {
    if (err) throw err;
    console.log(user);
    res.render("user-info", { user });
  });
});


module.exports = router;
