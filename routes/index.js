const express = require("express");
const router = express.Router();
const path = require("path");

router.get("/", (req, res) => {
  res.render("index");
});

router.get("/asset/:cat/:name", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "public", req.params.cat, req.params.name))
})

router.get("/asset/:cat/:cat2/:name", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "public", req.params.cat, req.params.cat2, req.params.name))
})

router.get("/asset/:cat/:cat2/:cat3/:name", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "public", req.params.cat, req.params.cat2, req.params.cat3, req.params.name))
})

router.get("/asset/:cat/:cat2/:cat3/:cat4/:name", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "public", req.params.cat, req.params.cat2, req.params.cat3, req.params.cat4, req.params.name))
})

module.exports = router;
