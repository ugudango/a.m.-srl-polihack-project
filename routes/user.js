const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const User = require("./../models/user");
const session = require("express-session");

router.get("/login", (req, res) => {
  if (req.session && req.session.userId) {
    res.redirect("/dashboard");
  } else res.render("login");
});

router.get("/register", (req, res) => {
  res.render("register");
});

router.post("/register", (req, res) => {
  let newUser = new User({
    username: req.body.candidateUser.username,
    password: req.body.candidateUser.password
  });

  User.createUser(newUser, err => {
    if (err)
      //Flash maybe?
      res.send(err);
    else res.status(200).send({ message: "User created successfully!" });
  });
});

router.post("/login", (req, res) => {
  if (req.session && req.session.userId) {
    res.redirect("/dashboard");
  } else {
    let candidateUser = {
      username: req.body.username,
      password: req.body.pass
    };
    User.authenticateUser(
      candidateUser,
      (err, verifiedUser, message, userId) => {
        if (err) throw err;
        if (verifiedUser == false) res.status(403).send(message);
        else {
          console.log(userId);
          req.session.userId = userId;
          req.session.save(err => {
            if (err) throw err;
            return res.redirect("/dashboard");
          });
        }
      }
    );
  }
});

router.get("/logout", (req, res, next) => {
  if (req.session) {
    req.session.destroy(function(err) {
      if (err) {
        return next(err);
      } else {
        res.clearCookie("connect.sid");
        return res.redirect("/");
      }
    });
  }
});

module.exports = router;
