$(document).ready(function() {
  $(".login100-form-btn").on("click", this, function(event) {
    event.preventDefault();
    if ($(".username100").val() != "" && $(".password100").val() != "") {
      var registerData = {
        candidateUser: {
          username: $(".username100").val(),
          password: $(".password100").val()
        }
      };
      
      $.ajax({
        method: "POST",
        url: "/user/register",
        
        dataType: "json",
        contentType: "application/json",
        statusCode: {
          200: function() {
            console.log("Successfully registered!");
            window.location.replace("/user/login");
          },
          403: function() {
            console.log("User already exists!");
          }
        },
        data: JSON.stringify(registerData),
        processData: false
      });
    }
  });
});
